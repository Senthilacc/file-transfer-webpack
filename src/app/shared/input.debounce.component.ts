import {Component,OnInit, Input, Output, ElementRef, EventEmitter} from '@angular/core';  
import { CommonModule } from '@angular/common';
import {Observable} from 'rxjs/Rx';

@Component({
    selector: 'input-debounce',
    templateUrl: './input.debounce.component.html'
})
export class InputDebounceComponent {  
    @Input() placeholder: string;
    @Input() delay: number = 300;
    @Input('initialValue') initialValue:string ='';
    @Output() value: EventEmitter<String> = new EventEmitter<String>();
       
inputValue: string;
    constructor(private elementRef: ElementRef) {
        console.log('initialValue'+this.placeholder);
        const eventStream = Observable.fromEvent(elementRef.nativeElement, 'keyup')
            .map(() => this.inputValue)
            .debounceTime(this.delay)
            .distinctUntilChanged();

        eventStream.subscribe(input => this.value.emit(input));
    }
    
     ngOnInit() {
       this.inputValue= this.initialValue;
    }
    
    clearValue():void {
        this.inputValue = '';
         this.value.emit('');
    }
    
}