export interface IEmailConfig {   
    toAddr: string;
    fromAddr: string;
    ccAddr: string;
    bccAddr: string;
    subject: boolean;
    body: string;  
}
