// counter.ts
import { ActionReducer, Action } from '@ngrx/store';
import { browseState } from '../models/browseState'

export const SEARCH = 'SEARCH';
export const CLEAR = 'CLEAR';
export const PAGECHANGE = 'PAGECHANGE';

export const initialState: browseState = {
  'filterKey':'',
  'pageNumber':1,
  'env': 'DEV'
};

export function browseReducer(state: browseState=initialState, action: Action):browseState {
	switch (action.type) {
		case SEARCH:
			return {
				 	'filterKey':action.payload.filterKey,
			        'pageNumber':action.payload.pageNumber,
					'env':state.env
				};

		case CLEAR:
			return {
				 	'filterKey':'',
			        'pageNumber':1,
					'env':state.env
				};
				
		case 'ENV_CHANGE':
			return {
				 	'filterKey':state.filterKey,
			        'pageNumber':state.pageNumber,
					'env':action.payload.env
				};

		default:
			return state;
	}
}