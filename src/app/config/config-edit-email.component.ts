import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IConfig } from './config';

@Component({
    templateUrl: './config-edit-email.component.html'
})
export class ConfigEditEmailComponent implements OnInit {
    errorMessage: string;
    newTags = '';
    config: IConfig;

    constructor(private route: ActivatedRoute) { }

    ngOnInit(): void {
        this.route.parent.data.subscribe(data => {
            this.config = data['config'];
        });
    }

    // Add the defined tags
    addTags(): void {
        let tagArray = this.newTags.split(',');
     //   this.config.tags = this.config.tags ? this.config.tags.concat(tagArray) : tagArray;
        this.newTags = '';
    }

    // Remove the tag from the array of tags.
    removeTag(idx: number): void {
        
        //this.config.tags.splice(idx, 1);
    }
    
       
    searchConfig(value: string): void{
        console.log(value);
    }
}
