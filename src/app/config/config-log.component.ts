import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ITransactionLog } from './transactionLog';
import { ConfigService } from './config.service';

import { IConfig } from './config';


@Component({
    templateUrl: './config-log.component.html'
})
export class ConfigLogComponent implements OnInit {
    pageTitle: string = 'Transaction Log Details';  
    config: IConfig;
    id: any;
    logs: ITransactionLog[] = new Array<ITransactionLog>();
    loading: boolean;
    
    constructor(private route: ActivatedRoute,
    private configService: ConfigService) {
        this.config = this.route.snapshot.data['config'];
       this.id = this.route.params['_value'].id;
        this.getLogs();
     }

    ngOnInit(): void {
       
        
    }
    
    getLogs():void {
        this.loading = true;
        this.configService.searchLogsByInterfaceId(this.id).subscribe( data =>
            {
               this.loading = false;
                this.logs = data;
            }
        );
    }
    
    refresh(): void {
        this.getLogs();
    }
}
