import { Injectable } from '@angular/core';
import { Http, URLSearchParams, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { browseState } from './models/browseState'
import { Store } from '@ngrx/store';
import {IPAddress} from '../shared/environment';

import {IConfig} from './config';
import {ITransactionLog} from './transactionLog';

@Injectable()
export class ConfigService {
    private baseUrl = 'api/products';
    
     store: Store<browseState>;
     env: string;
     ip: string;
     
     private baseUrl_filetransfer = ':8090/filetransferservice/api';
     
    constructor(private http: Http,
                private _store: Store<browseState>) {
                    this.store = _store;
                    
                     _store.select('browse').subscribe((obj: browseState) => 
                        {                           
                            this.env =  obj.env; 
                            this.ip = IPAddress[this.env];
                            console.log('Config-Service:'+ this.getBaseURL());
                        }
                    );
                    
                 }
 
    getConfigs(): Observable<IConfig[]> {
        
        //  let params: URLSearchParams = new URLSearchParams();
        //  params.set('configName', '');
        //  let options = new RequestOptions();
        //  options.search = params;
        
        return this.http.get(this.getBaseURL() + '/configs')
            .map(this.extractData)
            //.do(data => console.log('getConfigs: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }
   
    getConfigByName(input: string): Observable<IConfig[]>{
        
         let params: URLSearchParams = new URLSearchParams();
         params.set('configName', input);
         let options = new RequestOptions();
         options.search = params;
        
        const url = this.getBaseURL() + '/config';
        return this.http.get(url,options)
            .map(this.extractData)
            .do(data => console.log('getConfig: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getConfig(id: number): Observable<IConfig[]> {
        if (id === 0) {
            return Observable.of(this.initializeProduct());
        };
        const url = this.getBaseURL() + '/config/'+ id;
        return this.http.get(url)
            .map(this.extractData)
            .do(data => console.log('getConfig: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    deleteProduct(id: number): Observable<Response> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = this.getBaseURL() +`/config/${id}`;
        return this.http.delete(url, options)
            .do(data => console.log('deleteProduct: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    saveProduct(product: IConfig): Observable<IConfig> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers});
        console.log(product.id);
        if (product.id === 0) {
            return this.createProduct(product, options);
        }
        return this.updateProduct(product, options);
    }

    private createProduct(product: IConfig, options: RequestOptions): Observable<IConfig> {
        product.id = undefined;
         const url = this.getBaseURL() + '/config'
        return this.http.post(url, product, options)
            .map(this.extractData)
          //  .do(data => console.log('createConfig: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private updateProduct(product: IConfig, options: RequestOptions): Observable<IConfig> {
        const url = this.getBaseURL() + '/config/'+ product.id
        return this.http.put(url, product,options)
            .map(() => product)
         //   .do(data => console.log('updateConfig: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }
    
     public searchLogsByInterfaceId(interfaceId: number): Observable<ITransactionLog[]> {
         
          let params: URLSearchParams = new URLSearchParams();
         params.set('interfaceId', interfaceId.toString());
         let options = new RequestOptions();
         options.search = params;                
         
        const url = this.getBaseURL() + '/logs'      
        return this.http.get(url,options)
        .map(this.extractData)        
        .catch(this.handleError);
    }
    
    private getBaseURL(): string {
        return 'http://'+ this.ip + this.baseUrl_filetransfer;
      //return 'http://'+ 'localhost' + this.baseUrl_filetransfer
    }
    
    public reconfigure(interfaceId: number): any {
        const url = this.getBaseURL() + '/reconfigure/' + interfaceId;
        console.log(url);
         return this.http.get(url)
        .map(this.extractData)
        .do(data=> console.log('reconfigure Logs:'+ JSON.stringify(data)))        
        .catch(this.handleError);
    }

    private extractData(response: Response) {
        let body = response.json();
        return body.data || {};
    }



    private handleError(error: Response): Observable<any> {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json().error || 'Unexpected Error Occurred during API Call.');
    }

    initializeProduct(): IConfig[] {
       
        return [{
            id: 0,
            interfaceName: null,
          
    sourceAddress: null,
    destinationAddress: null,
   // tags?: string[];
    sourceArchiveAddress: null,
    activeInd: null,
    localArchiveAddress: null,
    cronExpression: null,
    processMultipleFiles: null,
    regexFileNameFilterExpression: null,
    compressFileBeforeTransfer: null,
    renameFileSuffixPattern: null,
    emailacknowledgementRequired: null,
    emailInfo: {    
    toAddr: null,
    fromAddr: null,
    ccAddr: null,
    bccAddr: null,
    subject:null,
    body:null
    }
        }];
    }
}
