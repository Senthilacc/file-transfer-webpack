import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';
import { ConfigListComponent } from './config-list.component';

@Injectable()
export class ConfigListGuard implements CanDeactivate<ConfigListComponent> {
   
   constructor(private localStorageService: LocalStorageService) {
       
   }

    canDeactivate(component: ConfigListComponent): boolean {      
      this.localStorageService.set('filterkey',component.filteredValue);
      this.localStorageService.set('pageNumber',component.p);
        return true;
    }
}
