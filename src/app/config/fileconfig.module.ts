import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ConfigListComponent } from './config-list.component';
import { ConfigDetailComponent } from './config-detail.component';
import { ConfigEditComponent } from './config-edit.component';
import { ConfigEditInfoComponent } from './config-edit-info.component';
import { ConfigEditEmailComponent } from './config-edit-email.component';
import { ConfigLogComponent } from './config-log.component';
import {ConfigListGuard} from './config-list.guard';

import { ConfigFilterPipe } from './config-filter.pipe';
import { ConfigService } from './config.service';
import { ConfigResolver } from './config-resolver.service';
import { ConfigEditGuard } from './config-guard.service';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { browseReducer } from './reducers/browse';

import { AppReducer } from '../reducers/AppReducer';

import {NgxPaginationModule} from 'ngx-pagination';

import { SharedModule } from '../shared/shared.module'; 

@NgModule({
  imports: [
    SharedModule,
    NgxPaginationModule,
    StoreModule.provideStore({ browse: browseReducer,appState:AppReducer }),
    
     StoreDevtoolsModule.instrumentOnlyWithExtension({
      maxAge: 5
    }),
    RouterModule.forChild([
      {
        path: '',
        component: ConfigListComponent,
         canDeactivate: [ConfigListGuard]
      },
       {
        path: ':id/logs',
        component: ConfigLogComponent,
        resolve: { config: ConfigResolver }
      },
      {
        path: ':id',
        component: ConfigDetailComponent,
        resolve: { config: ConfigResolver }
      },
      {
        path: ':id/edit',
        component: ConfigEditComponent,
        resolve: { config: ConfigResolver },
        canDeactivate: [ConfigEditGuard],
        children: [
          {
            path: '',
            redirectTo: 'info',
            pathMatch: 'full'
          },
          {
            path: 'info',
            component: ConfigEditInfoComponent
          },
          {
            path: 'tags',
            component: ConfigEditEmailComponent
          }
        ]
      }
    ])
  ],
  declarations: [
    ConfigListComponent,
    ConfigDetailComponent,
    ConfigEditComponent,
    ConfigEditInfoComponent,
    ConfigEditEmailComponent,
    ConfigFilterPipe,
    ConfigLogComponent
  ],
  providers: [
    ConfigService,
    ConfigResolver,
    ConfigEditGuard,
    ConfigListGuard
  ]
})
export class FileConfigModule { }
