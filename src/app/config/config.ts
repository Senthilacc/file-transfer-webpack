import {IEmailConfig} from './emailConfig'


export interface IConfig {
    id: number;
    interfaceName: string;
    sourceAddress: string;
    destinationAddress: string;
    sourceArchiveAddress: string;
    activeInd: boolean;
    localArchiveAddress: string;
    cronExpression: string;
    processMultipleFiles: boolean;
    regexFileNameFilterExpression: string;
    compressFileBeforeTransfer: boolean;
    renameFileSuffixPattern: string;
    emailacknowledgementRequired: boolean;
    emailInfo: IEmailConfig;
    //imageUrl: string; 
}
