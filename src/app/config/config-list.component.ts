import { Component, OnInit, AfterViewInit }  from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs/Observable';

import { IConfig } from './config';
import { ConfigService } from './config.service';
import { LocalStorageService } from 'angular-2-local-storage';
import { browseState } from './models/browseState'
import { Store } from '@ngrx/store';
import { INCREMENT, DECREMENT, RESET } from './counter';
import { SEARCH, PAGECHANGE, CLEAR } from './reducers/browse';
import {AppState} from '../models/AppState';
import {NgxPaginationModule,PaginationControlsComponent,PaginationService,PaginationInstance} from 'ngx-pagination';


@Component({
    templateUrl: './config-list.component.html',
    styleUrls: ['./config-list.component.css']
})
export class ConfigListComponent implements OnInit,AfterViewInit {
    pageTitle: string = 'Search File Transfer Config';
    imageWidth: number = 50;
    imageMargin: number = 2;
    showImage: boolean = false;
    listFilter: string;
    errorMessage: string;
    filteredValue: string;  
    loading: boolean;
    configs: IConfig[];
    pagination: PaginationInstance;  
    service: PaginationService;
    p: Number = 1;   
    state: browseState;
    store: Store<browseState>;
    env: string;

    constructor(private configService: ConfigService,
                private route: ActivatedRoute,
                private localStorageService: LocalStorageService,
                private _service: PaginationService,
                private _store: Store<browseState>,
                private _appStore: Store<AppState>) {
                    this.service = _service;
                    this.store = _store;
                    
                    
                     
                 //     _appStore.select('appState').subscribe(obj =>
                //        this.updateStore()                    );                   
                   
                //    _appStore.dispatch({'type':'ENV_CHANGE',payload:{'env':this.env}});
                    
                    _store.select('browse').subscribe((obj: browseState) => 
                        {
                            this.state = obj;                        
                            this.filteredValue = this.state.filterKey;
                            this.p = this.state.pageNumber;
                            this.env =  this.state.env; 
                        }
                    );     
                    
                    route.queryParams.subscribe(params => {
                             
                      let environment = params['env'];
                       if(environment){
                      this.store.dispatch({ type: 'ENV_CHANGE',payload:{
                        'env':environment
                    }});
                    
                    this.store.dispatch({type: CLEAR});
                    
                   this.loading = true;
                     this.searchConfig(this.filteredValue);
                     
                     
            }
                      
                    });    
                 
                 }

    toggleImage(): void {
        this.showImage = !this.showImage;
    }

    ngOnInit(): void {
       this.loading = true;  
     this.getListOfConfigs();
        
    }
    ngAfterViewInit() {
       
    }
    
    updateStore(): void {
      
         this.store.dispatch({ type: SEARCH,payload:{
           'filterKey':this.filteredValue,
		   'pageNumber':this.p
       }});
    }
    
    searchConfig(value: string): void{
       console.log('searchConfig');
        this.clearError();
        this.filteredValue = value;
        this.loading = true;
          this.configService.getConfigByName(value)
                .subscribe(configs => { this.configs = configs; this.loading=false;},
                           error => {this.errorMessage = <any>error;
                               this.configs = []
                               this.loading = false;});
    }
    
    reconfigure(id: number): void {
        
        this.configService.reconfigure(id).subscribe(
            (data: any) => console.log(JSON.stringify(data)));
    }
    
    clearError(): void{
        this.errorMessage = null;
    }
   
   getPaginationInstance(): PaginationInstance{
       return this._service.getInstance();
   }
   
  getListOfConfigs():void{
        this.clearError();
        
       if(this.filteredValue)
            this.searchConfig(this.filteredValue);
        else
            this.configService.getConfigs()
            .subscribe(configs => {this.loading = false; 
                this.configs = configs},
                        error => {this.loading = false; 
                            this.errorMessage = <any>error});
  }
  
}
