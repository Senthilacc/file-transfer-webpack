// counter.ts
import { ActionReducer, Action } from '@ngrx/store';
import { AppState } from '../models/AppState'

export const ENV_CHANGE = 'ENV_CHANGE';
export const CLEAR = 'CLEAR';
export const PAGECHANGE = 'PAGECHANGE';

export const initialState: AppState = {
 'env':'dev'
};

export function AppReducer(state: AppState=initialState, action: Action):AppState {
     console.log('App State' + JSON.stringify(state));
	switch (action.type) {
		case ENV_CHANGE:
       
			return {
				 	'env':action.payload.env
				};

		case CLEAR:
			return {
				 	'env':'dev'
				};

		default:
			return state;
	}
}