import { NgModule } from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { WelcomeComponent } from './home/welcome.component';
import { PageNotFoundComponent } from './page-not-found.component';
import { AppComponent } from './app.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { LocalStorageModule } from 'angular-2-local-storage';

import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import {AppReducer} from './reducers/AppReducer'

/* Feature Modules */
import { UserModule } from './user/user.module';
import { MessageModule } from './messages/message.module';

//import { FileConfigModule } from './config/fileconfig.module';


@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
     UserModule,
    MessageModule,
       StoreModule.provideStore({ appState: AppReducer }),
      StoreDevtoolsModule.instrumentOnlyWithExtension({
       maxAge: 5
     }),
    AppRoutingModule,
   // FileConfigModule,
    LocalStorageModule.withConfig({
            prefix: 'my-app',
          storageType: 'localStorage'
          //  storageType: 'sessionStorage'
        }),
    NgxPaginationModule
  ],
  declarations: [
    AppComponent,WelcomeComponent,PageNotFoundComponent
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
