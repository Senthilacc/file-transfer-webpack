//import { Component } from '@angular/core';

//import '../assets/css/styles.css';

/**@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent { }*/


import { Component } from '@angular/core';
import { Router, ActivatedRoute, Event, NavigationStart, NavigationEnd, NavigationError, NavigationCancel } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';
import { AuthService } from './user/auth.service';
import { MessageService } from './messages/message.service';


import { Store } from '@ngrx/store';
import {AppState} from './models/AppState';


@Component({
    selector: 'my-app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    pageTitle: string = 'ESB Management';
    loading: boolean = true;
    collection:Array<any> = [];
    env: string = 'DEV'
    appState: AppState;
    appStore: Store<AppState>;
    
    constructor(private authService: AuthService,
                private messageService: MessageService,
                private localStorageService: LocalStorageService,
                private router: Router,
                 private route: ActivatedRoute,
                 private _appStore: Store<AppState>) {
        this.localStorageService.clearAll();
        router.events.subscribe((routerEvent: Event) => {
            this.checkRouterEvent(routerEvent);
        });
       
       route.queryParams.subscribe(params => {
                                              
                        
                      let environment = params['env'];
                       if(environment){
                      this.env= environment;
                  
                   
                    
            }
                      
                    });
        
        
        for (let i = 1; i <= 100; i++) {
      this.collection.push(`item ${i}`);
    }
    }

    checkRouterEvent(routerEvent: Event): void {
        if (routerEvent instanceof NavigationStart) {
            this.loading = true;
        }

        if (routerEvent instanceof NavigationEnd ||
            routerEvent instanceof NavigationCancel ||
            routerEvent instanceof NavigationError) {
            this.loading = false;
        }
    }

    displayMessages(): void {
        // Example of primary and secondary routing together
        // this.router.navigate(['/login', {outlets: { popup: ['messages']}}]); // Does not work
        // this.router.navigate([{outlets: { primary: ['login'], popup: ['messages']}}]); // Works
        this.router.navigate([{outlets: { popup: ['messages']}}]); // Works
        this.messageService.isDisplayed = true;
    }

    hideMessages(): void {
        this.router.navigate([{ outlets: { popup: null } }]);
        this.messageService.isDisplayed = false;
    }

    logOut(): void {
        this.authService.logout();
        this.router.navigateByUrl('/login');
    }
    clicked(event:any,value:string):void{
        event.preventDefault();
       this.env= value;
      // this.appStore.dispatch({'type':'ENV_CHANGE',payload:{'env':value}});
       
       this.router.navigate(['/'],{queryParams:{'env':value}});
       
    }    

}
